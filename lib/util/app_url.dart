class AppUrl {
  static const String liveBaseURL =
      "https://shiny-awful-wildebeast.gigalixirapp.com/api/v1";
  static const String localBaseURL = "http://10.0.2.2:4000/api/v1";

  static const String baseURL = 'https://www.homsico.net/api/v1';
  static const String login = baseURL + "/login";
  static const String register = baseURL + "/register";
  static const String category = baseURL + "/categories?main=true";
  //static const String forgotPassword = baseURL + "/forgot-password";
}
