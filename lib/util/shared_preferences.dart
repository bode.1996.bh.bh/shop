import 'package:shared_preferences/shared_preferences.dart';
import 'package:t_mall_renew/models/response/loginresponse.dart';
import 'package:t_mall_renew/models/user.dart';

class UserPreferences {
  Future<bool> saveUser(Data user) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    // prefs.setString('api_token', user.name);
    // prefs.setInt('phone_number', user.phoneNumber);
    // prefs.setString('password', user.password);
    // prefs.setString('password_confirmation', user.passwordConfirmation);
    prefs.setString('api_token', user.apiToken);
    print(user.apiToken);

    print("object prefere");
    //print(user.fcmToken);
    return prefs.commit();
  }

  // Future<bool> savelog(Data data) async {
  //   final SharedPreferences prefs = await SharedPreferences.getInstance();

  //   prefs.setString('api_token', data.apiToken);

  //   print("object prefere");
  //   print(data.apiToken);
  // }

  Future<Data> getUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    // String name = prefs.getString('name');
    // int phoneNumber = prefs.getInt('phone_number');
    // String password = prefs.getString('password');
    // String passwordConfirmation = prefs.getString('password_confirmation');
    String apiToken = prefs.getString('api_token');
    return Data(
      // name: name,
      // phoneNumber: phoneNumber,
      // password: password,
      // passwordConfirmation: passwordConfirmation,
      apiToken: apiToken,
    );
  }

  void removeUser() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    // prefs.remove('name');
    // prefs.remove('phone_number');
    // prefs.remove('password');
    prefs.remove('api_token');
  }

  Future<String> getToken(args) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('api_token');
    return token;
  }
}
