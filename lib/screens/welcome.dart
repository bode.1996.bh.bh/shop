import 'package:flutter/material.dart';
import 'package:t_mall_renew/models/response/loginresponse.dart';
import 'package:t_mall_renew/models/user.dart';

class Welcome extends StatelessWidget {
  final User user;
  final Data data;

  const Welcome({Key key, this.user, this.data}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Center(
          child: Text('Welcome page'),
        ),
      ),
    );
  }
}
