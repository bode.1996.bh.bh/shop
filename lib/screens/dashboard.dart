import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:t_mall_renew/models/categories/main_categories.dart';

import 'package:t_mall_renew/models/response/loginresponse.dart';

import 'package:t_mall_renew/providers/auth.dart';
import 'package:t_mall_renew/providers/category.dart';
import 'package:t_mall_renew/providers/user.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  Cate cate;
  Category category;
  Future<List<dynamic>> fetchList;
  List prod = [];
  int prodCounts;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Data user = Provider.of<UserProvider>(context).user;

    return Scaffold(
        appBar: AppBar(
          title: Text('Dashboard'),
        ),
        body: FutureBuilder<Cate>(
          future:
              Provider.of<CategoryProvider>(context, listen: false).getList(),
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
                return CircularProgressIndicator();
              default:
                if (snapshot.hasError)
                  return Text('Error: ${snapshot.error}');
                else {
                  cate = snapshot.data;
                  print(cate.toString());
                  // print(cate.nameEn.toString());
                  return Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Center(child: Text(cate.nameEn ?? 'default')),
                    ],
                  );
                }
            }
          },
        ));
  }
}
