import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:t_mall_renew/constants/buttons.dart';
import 'package:t_mall_renew/constants/fields.dart';
import 'package:t_mall_renew/models/user.dart';
import 'package:t_mall_renew/providers/auth.dart';
import 'package:t_mall_renew/providers/user.dart';

class Register extends StatefulWidget {
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  final formKey = new GlobalKey<FormState>();
  String _username, _password, _name;
  @override
  Widget build(BuildContext context) {
    AuthProvider auth = Provider.of<AuthProvider>(context);
    var doRegister = () {
      final form = formKey.currentState;
      //if (form.validate()) {
      form.save();
      auth
          .register(
        _name,
        _username,
        _password,
      )
          .then((value) {
        if (value['status']) {
          User user = value['data'];
          //Provider.of<UserProvider>(context, listen: false).setUser(user);
          Navigator.pushReplacementNamed(context, '/dashboard');
        } else {
          Flushbar(
            title: 'Registration Failed',
            message: value.toString(),
            duration: Duration(seconds: 10),
          ).show(context);
        }
      });
      // }
    };
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(40.0),
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 215.0),
                  label("Phine"),
                  SizedBox(height: 5.0),
                  name(
                      onSaved: (value) => _name = value,
                      validator: (value) => value.isEmpty ? 'please' : null),
                  SizedBox(height: 15.0),
                  userNameField(
                      onSaved: (value) => _username = value,
                      validator: (value) => value.isEmpty ? 'please' : null),
                  SizedBox(height: 15.0),
                  passwordField(
                      onSaved: (value) => _password = value,
                      validator: (value) => value.isEmpty ? 'please' : null),
                  SizedBox(height: 15.0),
                  conpasswordField(
                      onSaved: (value) => _password = value,
                      validator: (value) => value.isEmpty ? 'please' : null),
                  SizedBox(
                    height: 50,
                  ),
                  auth.registeredInStatus == Status.Registering
                      ? loading()
                      : longButton(title: "register", onPressed: doRegister)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
