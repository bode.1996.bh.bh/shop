import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:t_mall_renew/constants/buttons.dart';
import 'package:t_mall_renew/constants/fields.dart';
import 'package:t_mall_renew/models/response/loginresponse.dart';
import 'package:t_mall_renew/models/user.dart';
import 'package:t_mall_renew/providers/auth.dart';
import 'package:t_mall_renew/providers/user.dart';
import 'package:t_mall_renew/util/validators.dart';

class LogIn extends StatefulWidget {
  @override
  _LogInState createState() => _LogInState();
}

class _LogInState extends State<LogIn> {
  final formKey = new GlobalKey<FormState>();
  String _phone, _password;

  @override
  Widget build(BuildContext context) {
    AuthProvider authProvider = Provider.of<AuthProvider>(context);
    var doLogin = () {
      final form = formKey.currentState;
      //if (form.validate()) {
      form.save();
      final Future<Map<String, dynamic>> successfulMessage = authProvider.login(
        _phone,
        _password,
      );
      successfulMessage.then((response) {
        if (response['status']) {
          //print(response['status']);
          Data user = response['user'];
          Provider.of<UserProvider>(context, listen: false).setUser(user);
          Navigator.pushReplacementNamed(context, '/dashboard');
        } else {
          Flushbar(
            title: 'Failed login',
            message: response['message'].toString() ?? ['dog'],
            duration: Duration(seconds: 3),
          ).show(context);
        }
      });
      //  else {
      //   print('form is valid');
      // }
    };

    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.all(40.0),
            child: Form(
              key: formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 250,
                  ),
                  label('Phone'),
                  SizedBox(
                    height: 5,
                  ),
                  userNameField(
                      onSaved: (value) => _phone = value,
                      validator: validateEmail),
                  SizedBox(
                    height: 10,
                  ),
                  passwordField(
                    onSaved: (value) => _password = value,
                    validator: (value) =>
                        value.isEmpty ? "Please enter password" : null,
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  authProvider.loggedInStatus == Status.Authenticating
                      ? loading()
                      : longButton(title: 'login', onPressed: doLogin),
                  SizedBox(
                    height: 5,
                  ),
                  buttonsRaw(context),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
