import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:t_mall_renew/models/response/loginresponse.dart';

import 'package:t_mall_renew/providers/auth.dart';
import 'package:t_mall_renew/providers/category.dart';
import 'package:t_mall_renew/providers/user.dart';
import 'package:t_mall_renew/router/router.dart';
import 'package:t_mall_renew/screens/dashboard.dart';
import 'package:t_mall_renew/screens/login.dart';
import 'package:t_mall_renew/screens/welcome.dart';
import 'package:t_mall_renew/util/shared_preferences.dart';

void main() {
  runApp(MyApp(
    route: AppRoute(),
  ));
}

class MyApp extends StatelessWidget {
  final AppRoute route;

  const MyApp({this.route});
  @override
  Widget build(BuildContext context) {
    Future<Data> getUserData() => UserPreferences().getUser();
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (_) => AuthProvider()),
        ChangeNotifierProvider(create: (_) => UserProvider()),
        ChangeNotifierProvider(create: (_) => CategoryProvider()),
      ],
      child: MaterialApp(
        title: 'Mall',
        theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity),
        home: FutureBuilder(
          future: getUserData(),
          builder: (context, snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.none:
              case ConnectionState.waiting:
                return CircularProgressIndicator();
              default:
                if (snapshot.hasError)
                  return Text('Error: ${snapshot.error}');
                else if (snapshot.data.apiToken == null)
                  return Dashboard();
                else
                  UserPreferences().removeUser();
                return Welcome(data: snapshot.data);
            }
          },
        ),
        onGenerateRoute: route.generateRoute,
      ),
    );
  }
}
