import 'package:flutter/material.dart';
import 'package:t_mall_renew/constants/decorations.dart';

TextFormField name({Function onSaved, Function validator}) {
  return TextFormField(
    autofocus: false,
    validator: validator,
    onSaved: onSaved,
    decoration: buildInputDecoration('abd@abd.com', Icons.email),
  );
}

TextFormField userNameField({Function onSaved, Function validator}) {
  return TextFormField(
    autofocus: false,
    validator: validator,
    onSaved: onSaved,
    decoration: buildInputDecoration('abd@abd.com', Icons.email),
  );
}

TextFormField passwordField({Function onSaved, Function validator}) {
  return TextFormField(
    obscureText: true,
    autofocus: false,
    validator: validator,
    onSaved: onSaved,
    decoration: buildInputDecoration('123456', Icons.lock),
  );
}

TextFormField conpasswordField({Function onSaved, Function validator}) {
  return TextFormField(
    obscureText: true,
    autofocus: false,
    validator: validator,
    onSaved: onSaved,
    decoration: buildInputDecoration('123456', Icons.lock),
  );
}

Row loading() {
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: <Widget>[
      CircularProgressIndicator(),
      Text('Authenticating ... please wait'),
    ],
  );
}
