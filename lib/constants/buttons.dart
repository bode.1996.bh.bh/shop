import 'package:flutter/material.dart';

Row buttonsRaw(context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceBetween,
    children: <Widget>[
      flatButton(text: Text('Forget Password?'), onPressed: () {}),
      flatButton(
          text: Text(
            'sign Up',
            style: TextStyle(fontWeight: FontWeight.w300),
          ),
          onPressed: () {
            Navigator.pushReplacementNamed(context, '/register');
          }),
    ],
  );
}

FlatButton flatButton({Text text, Function onPressed}) {
  return FlatButton(
    padding: EdgeInsets.all(0.0),
    onPressed: onPressed,
    child: text,
  );
}

MaterialButton longButton(
    {String title,
    Function onPressed,
    Color color: const Color(0xfff063057),
    Color textColor: Colors.white}) {
  return MaterialButton(
    onPressed: onPressed,
    textColor: textColor,
    color: color,
    child: SizedBox(
      width: double.infinity,
      child: Text(
        title,
        textAlign: TextAlign.center,
      ),
    ),
    height: 45,
    minWidth: 600,
    shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(10))),
  );
}

label(String title) => Text(title);
