import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:t_mall_renew/screens/dashboard.dart';
import 'package:t_mall_renew/screens/login.dart';
import 'package:t_mall_renew/screens/register.dart';

class AppRoute {
  Route generateRoute(RouteSettings routeSettings) {
    switch (routeSettings.name) {
      case '/dashboard':
        return MaterialPageRoute(builder: (context) => Dashboard());
      case '/login':
        return MaterialPageRoute(builder: (_) => LogIn());
      case '/register':
        return MaterialPageRoute(builder: (context) => Register());
        break;
      default:
        return null;
    }
  }
}
