import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:t_mall_renew/models/categories/main_categories.dart';
import 'package:t_mall_renew/util/app_url.dart';

class CategoryProvider extends ChangeNotifier {
  Cate cate;
  Category category;

  Future<Cate> getList() async {
    final http.Response response =
        await http.get(Uri.parse(AppUrl.category), headers: {
      "Accept": "application/json",
      "content-type": "application/json",
    });
    if (response.statusCode == 200) {
      var responseJson = jsonDecode(response.body);
      //print(responseJson);
      cate = Cate.fromJson(responseJson);

      print(category.toJson());

      return Cate.fromJson(responseJson);
    } else
      return null;
  }
}
//Future<List> getList() async {
//   http.Response response = await http.get(Uri.parse(AppUrl.category));
//   if (response.statusCode == 200) {
//     final jsonResponse = jsonDecode(response.body);
//     final products = jsonResponse['data'];
//     List prod = products.map((i) => Cate.fromJson(i)).toList();
//     return prod;
//   } else {
//     throw Exception('failed to load');
//   }
// }
