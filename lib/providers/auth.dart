import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:t_mall_renew/models/categories/main_categories.dart';
import 'package:t_mall_renew/models/response/loginresponse.dart';
import 'package:t_mall_renew/models/user.dart';
import 'package:t_mall_renew/util/app_url.dart';
import 'package:t_mall_renew/util/shared_preferences.dart';

enum Status {
  NotLoggedIn,
  NotRegistered,
  LoggedIn,
  Registered,
  Authenticating,
  Registering,
  LoggedOut
}

class AuthProvider extends ChangeNotifier {
  Status _loggedInStatus = Status.NotLoggedIn;
  Status _registeredInStatus = Status.NotRegistered;
  Status get loggedInStatus => _loggedInStatus;
  Status get registeredInStatus => _registeredInStatus;

  Future<Map<String, dynamic>> login(
    String phone,
    String password,
  ) async {
    var results;
    final Map loginData = {
      'phone_number': phone,
      'password': password,
      'fcm_token':
          'qdGag0JbzXp7zUYI0OOYJ0x9DQiFYHqUfZBsoecE9bRhaN9ACsvCUbVgaaua'
    };
    _loggedInStatus = Status.Authenticating;
    notifyListeners();
    var response = await http.post(
      Uri.parse(AppUrl.login),
      body: json.encode(loginData),
      headers: <String, String>{
        'Content-type': 'application/json',
        'Accept': 'application/json',
        'Authorization':
            'qdGag0JbzXp7zUYI0OOYJ0x9DQiFYHqUfZBsoecE9bRhaN9ACsvCUbVgaaua'
      },
    );
    //print(response.body);
    if (response.statusCode == 200) {
      //print(response.statusCode);
      final responseData = jsonDecode(response.body);
      var userData = responseData['data'];

      print(userData.toString());
      Data authUser = Data.fromJson(userData);

      UserPreferences().saveUser(authUser);

      print(authUser.toJson());
      _loggedInStatus = Status.LoggedIn;
      notifyListeners();

      results = {'status': true, 'message': 'Successful', 'user': authUser};
    } else {
      _loggedInStatus = Status.NotLoggedIn;
      notifyListeners();
      results = {
        'status': false,
        'message': jsonDecode(response.body)['error']
      };
    }
    return results;
  }

  Future<Map<String, dynamic>> register(
    String name,
    String phone,
    String password,
  ) async {
    final Map<String, dynamic> registrationData = {
      'name': name,
      'phone_number': phone,
      'password': password,
      'password_confirmation': password,
      'fcm_token':
          'AAAA1BfW2bE:APA91bFpLvfxIe_uRQcHJhJFAllFyEmeL9k9NSqnYPQKqrj1QRr2Qe_xc976OsvZWCLTvwDv6gALAi8gu2K9B98XlVzVty8BLjvcnUGHM5RgsKyO1Yakj7iP8qGcHlkOd6oCd6XYEZBT',
    };
    _registeredInStatus = Status.Registering;

    notifyListeners();
    Map<String, String> headers = {
      'Content-Type': 'application/json;charset=UTF-8',
      'Charset': 'utf-8',
      "Accept": "application/json",
      "X-Requested-With": "XMLHttpRequest"
    };
    return await http
        .post(Uri.parse(AppUrl.register),
            body: jsonEncode(registrationData), headers: headers)
        .then(onValue)
        .catchError(onError);
  }

  static Future<FutureOr> onValue(http.Response response) async {
    var results;
    final Map<String, dynamic> responseData = jsonDecode(response.body);
    print(response.body);
    if (response.statusCode == 200 || response.statusCode == 201) {
      print(response.statusCode);
      var userData = responseData['data'];
      User authUser = User.fromJson(userData);
      //UserPreferences().saveUser(authUser);
      results = {
        'status': true,
        'message': 'Successfully resgistered',
        'data': authUser
      };
    } else {
      results = {
        'status': false,
        'message': 'Resgistration failed',
        'data': responseData
      };
    }
    return results;
  }

  static onError(error) {
    print('the user is $error.detail');
    return {'status': false, 'message': 'Unsuccessful Request', 'data': error};
  }

  // Future<List> getList() async {
  //   http.Response response = await http.get(Uri.parse(AppUrl.category));
  //   if (response.statusCode == 200) {
  //     final jsonResponse = jsonDecode(response.body);
  //     final products = jsonResponse['data'];
  //     List prod = products.map((i) => Cate.fromJson(i)).toList();
  //     return prod;
  //   } else {
  //     throw Exception('failed to load');
  //   }
  // }
}
