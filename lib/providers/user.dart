import 'package:flutter/cupertino.dart';
import 'package:t_mall_renew/models/response/loginresponse.dart';
import 'package:t_mall_renew/models/user.dart';

class UserProvider extends ChangeNotifier {
  Data _user = Data();

  Data get user => _user;
  void setUser(Data user) {
    _user = user;
    notifyListeners();
  }
}
