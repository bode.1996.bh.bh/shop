class Category {
  List<Cate> data;

  Category({this.data});

  Category.fromJson(Map<String, dynamic> json) {
    if (json['data'] != null) {
      data = new List<Cate>();
      json['data'].forEach((v) {
        data.add(new Cate.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Cate {
  int id;
  String nameAr;
  String nameEn;
  String code;
  String featuredImage;
  List<Children> children;
  Null sizeGuideImage;

  Cate(
      {this.id,
      this.nameAr,
      this.nameEn,
      this.code,
      this.featuredImage,
      this.children,
      this.sizeGuideImage});

  Cate.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameAr = json['name_ar'];
    nameEn = json['name_en'];
    code = json['code'];
    featuredImage = json['featured_image'];
    if (json['children'] != null) {
      children = new List<Children>();
      json['children'].forEach((v) {
        children.add(new Children.fromJson(v));
      });
    }
    sizeGuideImage = json['size_guide_image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_ar'] = this.nameAr;
    data['name_en'] = this.nameEn;
    data['code'] = this.code;
    data['featured_image'] = this.featuredImage;
    if (this.children != null) {
      data['children'] = this.children.map((v) => v.toJson()).toList();
    }
    data['size_guide_image'] = this.sizeGuideImage;
    return data;
  }
}

class Children {
  int id;
  String nameAr;
  String nameEn;
  String code;
  String featuredImage;
  List<Null> children;
  Null sizeGuideImage;

  Children(
      {this.id,
      this.nameAr,
      this.nameEn,
      this.code,
      this.featuredImage,
      this.children,
      this.sizeGuideImage});

  Children.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    nameAr = json['name_ar'];
    nameEn = json['name_en'];
    code = json['code'];
    featuredImage = json['featured_image'];

    sizeGuideImage = json['size_guide_image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['name_ar'] = this.nameAr;
    data['name_en'] = this.nameEn;
    data['code'] = this.code;
    data['featured_image'] = this.featuredImage;

    data['size_guide_image'] = this.sizeGuideImage;
    return data;
  }
}
