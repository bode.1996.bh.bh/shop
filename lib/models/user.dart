class User {
  String name;
  int phoneNumber;
  String password;
  String passwordConfirmation;
  String fcmToken;

  User(
      { this.name,
      this.phoneNumber,
      this.password,
      this.passwordConfirmation,
      this.fcmToken});

  User.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    phoneNumber = json['phone_number'];
    password = json['password'];
    passwordConfirmation = json['password_confirmation'];
    fcmToken = json['fcm_token'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['phone_number'] = this.phoneNumber;
    data['password'] = this.password;
    data['password_confirmation'] = this.passwordConfirmation;
    data['fcm_token'] = this.fcmToken;
    return data;
  }
}
